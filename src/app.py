from flask import Flask
import json
import time

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here
    ts = time.time()
    return json.dumps({"message": "Automate all the things!","timestamp": ts}, sort_keys=True, indent=4)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
