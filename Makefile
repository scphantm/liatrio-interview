
spinEnvironment1: tfInit tfPlan tfApply 
spinEnvironment2: dockerBuild dockerLogin dockerPush aksGetCreds helmUpgrade

dockerBuild: 
	docker buildx build --platform linux/amd64 -t wpsinterviewacr.azurecr.io/flaskapp:latest .
	
helmInstall:
	helm install flaskapp --create-namespace --namespace flaskapp -f helm/flaskapp/values.yaml helm/flaskapp

helmList:
	helm list --namespace flaskapp 
	
helmUpgrade:
	helm upgrade --reuse-values --install --create-namespace --namespace flaskapp flaskapp helm/flaskapp
	
helmUninstall:
	helm uninstall flaskapp --namespace flaskapp
	
aksGetCreds:
	az aks get-credentials -g rginterview -n aksinterview

tfInit:
	cd terraform; terraform init -upgrade

tfPlan:
	cd terraform; terraform plan

tfApply:
	cd terraform; terraform apply; \
	az aks update -n aksinterview -g rginterview --attach-acr wpsinterviewacr
	
dockerLogin:
	az acr login --name wpsinterviewacr; \
	docker login --username wpsinterviewacr wpsinterviewacr.azurecr.io
	
dockerPush:
	docker push --all-tags wpsinterviewacr.azurecr.io/flaskapp