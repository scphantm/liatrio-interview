FROM python:latest
LABEL authors="willie"

RUN mkdir -p /flaskapp/

COPY src/ /flaskapp/
WORKDIR /flaskapp/

RUN pip install -r requirements.txt

EXPOSE 5000
#CMD [ "flask", "run","--host","0.0.0.0","--port","5000"]
CMD [ "/usr/local/bin/python3", "app.py" ]
