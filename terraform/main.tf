

#resource "helm_release" "nginx_ingress" {
#  name       = "nginx-ingress-controller"
#
#  repository = "https://charts.bitnami.com/bitnami"
#  chart      = "nginx-ingress-controller"
#
#  set {
#    name  = "service.type"
#    value = "ClusterIP"
#  }
#}

data "azurerm_billing_mca_account_scope" "dinterview" {
  billing_account_name    = "9eb4664c-1cfe-5379-9707-3e41a131b993:f800dd37-7b3a-4f44-941c-67679a166216_2019-05-31"
  billing_profile_name = "5TOI-EL7C-BG7-PGB"
  invoice_section_name = "3G2I-RDHS-PJA-PGB"
}

#resource "azurerm_subscription" "interview" {
#  subscription_name = "InterviewSub"
#  billing_scope_id  = data.azurerm_billing_mca_account_scope.dinterview.id
#}

# Create a resource group
resource "azurerm_resource_group" "rginterview" {
  name     = "rginterview"
  location = "eastus"
}

#Create the aks cluster
resource "azurerm_kubernetes_cluster" "aksinterview" {
  name                = "aksinterview"
  location            = azurerm_resource_group.rginterview.location
  resource_group_name = azurerm_resource_group.rginterview.name
  dns_prefix          = "aksinterview"
  
  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.aksinterview.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.aksinterview.kube_config_raw

  sensitive = true
}

resource "azurerm_container_registry" "acr" {
  name                = "wpsinterviewacr"
  resource_group_name = azurerm_resource_group.rginterview.name
  location            = azurerm_resource_group.rginterview.location
  sku                 = "Premium"
  admin_enabled       = true
}

